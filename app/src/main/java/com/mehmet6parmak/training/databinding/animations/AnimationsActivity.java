package com.mehmet6parmak.training.databinding.animations;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.OnRebindCallback;
import androidx.databinding.ViewDataBinding;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.ViewGroup;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.databinding.ActivityAnimationsBinding;

public class AnimationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityAnimationsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_animations);

        binding.setLifecycleOwner(this);
        binding.setViewModel(new AnimationsViewModel());

        binding.addOnRebindCallback(new OnRebindCallback() {
            @Override
            public boolean onPreBind(ViewDataBinding binding) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot().findViewById(R.id.layout_onrebindcallback));
                }
                return super.onPreBind(binding);
            }
        });
    }
}
