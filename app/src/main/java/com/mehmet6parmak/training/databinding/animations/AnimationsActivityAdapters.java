package com.mehmet6parmak.training.databinding.animations;

import androidx.databinding.BindingAdapter;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

public class AnimationsActivityAdapters {

    @BindingAdapter("queueSize")
    public static void setQueueSize(TextView tv, int queueSize) {
        tv.setText(String.valueOf(queueSize));

        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000);
        tv.startAnimation(anim);
    }

}
