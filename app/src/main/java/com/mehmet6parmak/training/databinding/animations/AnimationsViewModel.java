package com.mehmet6parmak.training.databinding.animations;

import androidx.lifecycle.MutableLiveData;

import java.util.Timer;
import java.util.TimerTask;

public class AnimationsViewModel {

    public final MutableLiveData<Integer> queueSize = new MutableLiveData<>();
    public final MutableLiveData<Boolean> starVisible = new MutableLiveData<>();

    private Timer queueUpdaterTime = new Timer();

    public AnimationsViewModel() {
        queueSize.setValue(0);
        starVisible.setValue(true);

        queueUpdaterTime.schedule(new TimerTask() {
            @Override
            public void run() {
                queueSize.postValue(queueSize.getValue() + 1);
                starVisible.postValue(!starVisible.getValue());
            }
        }, 2000, 2000);
    }

}
