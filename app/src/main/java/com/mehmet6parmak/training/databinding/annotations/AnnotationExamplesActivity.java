package com.mehmet6parmak.training.databinding.annotations;

import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.databinding.ActivityAnnotationExamplesBinding;

public class AnnotationExamplesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityAnnotationExamplesBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_annotation_examples);
        AnnotationsViewModel viewModel = new AnnotationsViewModel();

        binding.setViewModel(viewModel);
    }
}
