package com.mehmet6parmak.training.databinding.annotations;

import com.mehmet6parmak.training.databinding.common.model.User;

import java.util.Date;

public class AnnotationsViewModel {

    public Date today = new Date();
    public User user = new User("Mehmet", "Altiparmak", 25);

}
