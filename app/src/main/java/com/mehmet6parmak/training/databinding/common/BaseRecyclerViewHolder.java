package com.mehmet6parmak.training.databinding.common;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.mehmet6parmak.training.databinding.BR;

public class BaseRecyclerViewHolder extends RecyclerView.ViewHolder {

    ViewDataBinding binding;

    public BaseRecyclerViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Object item) {
        binding.setVariable(BR.itemObject, item);
        binding.executePendingBindings();
    }

    public ViewDataBinding getBinding() {
        return binding;
    }
}
