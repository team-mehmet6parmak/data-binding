package com.mehmet6parmak.training.databinding.common;

import java.util.List;

/**
 * Check https://medium.com/google-developers/android-data-binding-recyclerview-db7c40d9f0e4 for details.
 */
public class SingleLayoutAdapter<DataType> extends BaseRecyclerViewAdapter {

    List<DataType> items;
    int layoutId;

    public SingleLayoutAdapter(List<DataType> items, int layoutId) {
        super();
        this.items = items;
        this.layoutId = layoutId;
    }

    public void updateData(List<DataType> newData) {
        this.items = newData;
        notifyDataSetChanged();
    }

    @Override
    protected Object getDataForPosition(int position) {
        return items.get(position);
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}
