package com.mehmet6parmak.training.databinding.common.binding.adapters;

import androidx.databinding.BindingConversion;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BindingConverters {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMMM, EEEE");

    @BindingConversion
    public static String setDate(Date date) {
        return DATE_FORMAT.format(date);
    }

}
