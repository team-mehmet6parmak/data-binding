package com.mehmet6parmak.training.databinding.common.binding.adapters;

import androidx.databinding.InverseMethod;

public final class Converter {

    private Converter() {
    }

    @InverseMethod("stringToInt")
    public static String intToString(int value) {
        return String.valueOf(value);
    }

    public static int stringToInt(String value) {
        return Integer.valueOf(value);
    }

}
