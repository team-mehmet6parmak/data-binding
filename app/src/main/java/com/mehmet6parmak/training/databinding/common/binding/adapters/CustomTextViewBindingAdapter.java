package com.mehmet6parmak.training.databinding.common.binding.adapters;

import androidx.databinding.BindingAdapter;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.databinding.InverseMethod;
import androidx.databinding.adapters.ListenerUtil;
import android.text.Editable;
import android.text.TextWatcher;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.common.views.CustomEditText;


//These are not necessary since getter/setter names are what the data binding library expects. Implemented here for demonstration
@InverseBindingMethods({
        @InverseBindingMethod(type = CustomEditText.class, attribute = "customText", method = "getCustomText")
})
@BindingMethods({
        @BindingMethod(type = CustomEditText.class, attribute = "customText", method = "setCustomText")
})
public class CustomTextViewBindingAdapter {


    /**
     * Note: Every two-way binding generates a synthetic event attribute. This attribute has the same
     * name as the base attribute, but it includes the suffix "AttrChanged". The synthetic event
     * attribute allows the library to create a method annotated using @BindingAdapter to associate
     * the event listener to the appropriate instance of View.
     */
    @BindingAdapter({"customTextAttrChanged"})
    public static void onCustomTextChanged(CustomEditText customEditText, final InverseBindingListener listener) {

        TextWatcher watcher = ListenerUtil.getListener(customEditText, R.id.customTextChangeListener);

        TextWatcher newWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listener.onChange();
            }
        };

        if (watcher != null) {
            customEditText.removeTextChangedListener(watcher);
        }

        customEditText.addTextChangedListener(newWatcher);
        ListenerUtil.trackListener(customEditText, newWatcher, R.id.customTextChangeListener);
    }


    /**
     * Used if the type in data model is different from type of property of widget.
     * In such situations we also need a BindingAdapter to be able to bind data to Widget.
     * <p>
     * Implementing both (BindingMethods, InverseBindingMethods) pair and (BindingAdapter, InverseBindingAdapter)
     * causes problems since they are intended for the same purpose.
     */

    /*
    @InverseBindingAdapter(attribute = "customText", event = "customTextAttrChanged")
    public String getCustomTextFromCustomEditText(CustomEditText edt) {
        return edt.getCustomText();
    }

    @BindingAdapter("customText")
    public void setCustomTextFromCustomEditText(CustomEditText edt, String text) {
        edt.setCustomText(text);
    }
    */


}
