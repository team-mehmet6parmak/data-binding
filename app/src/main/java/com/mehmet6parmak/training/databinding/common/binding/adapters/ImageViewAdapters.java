package com.mehmet6parmak.training.databinding.common.binding.adapters;

import androidx.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class ImageViewAdapters {

    @BindingAdapter(value = {"imageUrl", "placeHolder"}, requireAll = false)
    public static void loadImageAsync(ImageView imageView, String imageUrl, Drawable placeHolder) {
        if (TextUtils.isEmpty(imageUrl)) {
            imageView.setImageDrawable(placeHolder);
        } else {
            Glide.with(imageView.getContext())
                    .load(imageUrl)
                    .apply(RequestOptions.placeholderOf(placeHolder))
                    .into(imageView);
        }
    }

}
