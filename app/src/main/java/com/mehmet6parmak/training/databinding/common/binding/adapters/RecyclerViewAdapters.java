package com.mehmet6parmak.training.databinding.common.binding.adapters;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mehmet6parmak.training.databinding.common.SingleLayoutAdapter;
import com.mehmet6parmak.training.databinding.lists.UsersAdapter;

import java.util.List;

public class RecyclerViewAdapters {

    @BindingAdapter(value = {"users", "clickedListener"}, requireAll = false)
    public static void bindUsers(RecyclerView recyclerView, List<String> users, UsersAdapter.UserClickedListener listener) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter == null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            recyclerView.setAdapter(new UsersAdapter(users, listener));
        } else if (adapter instanceof SingleLayoutAdapter) {
            ((SingleLayoutAdapter) adapter).updateData(users);
        }
    }

}
