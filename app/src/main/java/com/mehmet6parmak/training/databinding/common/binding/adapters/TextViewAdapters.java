package com.mehmet6parmak.training.databinding.common.binding.adapters;

import android.content.res.Resources;
import androidx.databinding.BindingAdapter;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import android.widget.TextView;

@BindingMethods({
        @BindingMethod(type = TextView.class, attribute = "customTextColor", method = "setTextColor")
})
public final class TextViewAdapters {
    private TextViewAdapters() {
    }

    @BindingAdapter("android:text")
    public static void setText(TextView tv, int value) {
        try {
            String resource = tv.getContext().getResources().getString(value);
            tv.setText(resource);
        } catch (Resources.NotFoundException e) {
            tv.setText(String.valueOf(value));
        }
    }
}
