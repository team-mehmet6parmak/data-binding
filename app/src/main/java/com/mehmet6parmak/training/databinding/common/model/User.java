package com.mehmet6parmak.training.databinding.common.model;

public class User {

    private String firstName;
    private String lastName;
    private Integer age;
    private String profileImage = "https://www.gravatar.com/avatar/a2186c85ccf597e8b5bd63845cb3f0cd?s=328&d=identicon&r=PG";

    public User(String firstName, String lastName, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getProfileImage() {
        return profileImage;
    }
}
