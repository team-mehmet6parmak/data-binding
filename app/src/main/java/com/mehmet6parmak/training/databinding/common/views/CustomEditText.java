package com.mehmet6parmak.training.databinding.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.mehmet6parmak.training.databinding.R;

public class CustomEditText extends androidx.appcompat.widget.AppCompatEditText {

    CustomTextChangedListener customTextChangedListener;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
        String data = a.getString(R.styleable.CustomEditText_customText);
        a.recycle();

        setText(data);
    }

    public String getCustomText() {
        return getText().toString();
    }

    public void setCustomText(String text) {
        // Important to break potential infinite loops.
        if (!getCustomText().equals(text)) {
            String oldValue = getText().toString();
            setText(text);
            setSelection(text.length());

            onCustomTextChanged(oldValue, text);
        }
    }

    protected void onCustomTextChanged(String oldValue, String newValue) {
        if (customTextChangedListener != null) {
            customTextChangedListener.onCustomTextChanged(oldValue, newValue);
        }
    }

    public CustomTextChangedListener getCustomTextChangedListener() {
        return customTextChangedListener;
    }

    public void setCustomTextChangedListener(CustomTextChangedListener customTextChangedListener) {
        this.customTextChangedListener = customTextChangedListener;
    }

    //this is not necessary if the widget already have some ways to listen for changes such as TextWatchers in EditText.
    public interface CustomTextChangedListener {
        void onCustomTextChanged(String oldValue, String newValue);
    }

}
