package com.mehmet6parmak.training.databinding.home;

import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.databinding.ActivityHomeBinding;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityHomeBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        HomeViewModel vm = new HomeViewModel();
        binding.setViewModel(vm);
    }
}
