package com.mehmet6parmak.training.databinding.home;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.mehmet6parmak.training.databinding.animations.AnimationsActivity;
import com.mehmet6parmak.training.databinding.annotations.AnnotationExamplesActivity;
import com.mehmet6parmak.training.databinding.lists.ListOfUserActivity;
import com.mehmet6parmak.training.databinding.observables.ObservablesActivity;
import com.mehmet6parmak.training.databinding.simple.SimpleBindingActivity;
import com.mehmet6parmak.training.databinding.twoway.TwoWayBindingActivity;

import java.util.ArrayList;
import java.util.List;

public class HomeViewModel {

    private List<Section> sections;

    public HomeViewModel() {
        sections = new ArrayList<>();
        sections.add(new Section("Simple Binding", SimpleBindingActivity.class));
        sections.add(new Section("Annotations", AnnotationExamplesActivity.class));
        sections.add(new Section("List - 1", ListOfUserActivity.class));
        sections.add(new Section("Two Way Binding", TwoWayBindingActivity.class));
        sections.add(new Section("Observables", ObservablesActivity.class));
        sections.add(new Section("Animations", AnimationsActivity.class));
    }

    public List<Section> getSections() {
        return sections;
    }

    public View.OnClickListener getItemClickHandler() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Section section = (Section) v.getTag();
                Activity act = (Activity) v.getContext();

                act.startActivity(new Intent(act, section.getActivityClass()));
            }
        };
    }
}
