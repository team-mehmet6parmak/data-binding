package com.mehmet6parmak.training.databinding.home;

import android.app.Activity;

public class Section {

    private String title;
    private Class<? extends Activity> activityClass;

    public Section(String title, Class<? extends Activity> activityClass) {
        this.title = title;
        this.activityClass = activityClass;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class<? extends Activity> getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(Class<? extends Activity> activityClass) {
        this.activityClass = activityClass;
    }
}
