package com.mehmet6parmak.training.databinding.lists;

import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.databinding.ActivityListOfUserBinding;

public class ListOfUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityListOfUserBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_list_of_user);

        binding.setViewModel(new ListViewModel());
    }
}
