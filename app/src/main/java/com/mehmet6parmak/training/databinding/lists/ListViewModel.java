package com.mehmet6parmak.training.databinding.lists;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.mehmet6parmak.training.databinding.BR;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ListViewModel extends BaseObservable {

    private Timer timer = new Timer();
    private static int i = 0;

    private List<String> data = new ArrayList<>();

    public ListViewModel() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                data.add(String.valueOf("User - " + (i++ + 1)));
                notifyPropertyChanged(BR.data);
            }
        }, 1000, 1000);
    }

    @Bindable
    public List<String> getData() {
        return data;
    }

    public void onUserClicked() {

    }
}
