package com.mehmet6parmak.training.databinding.lists;

import androidx.annotation.NonNull;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.common.BaseRecyclerViewHolder;
import com.mehmet6parmak.training.databinding.common.SingleLayoutAdapter;

import com.mehmet6parmak.training.databinding.BR;

import java.util.List;

public class UsersAdapter extends SingleLayoutAdapter<String> {

    public interface UserClickedListener {
        void onClicked();
    }

    UserClickedListener listener;

    public UsersAdapter(List<String> items, UserClickedListener listener) {
        super(items, R.layout.item_user);
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseRecyclerViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.getBinding().setVariable(BR.clickedListener, listener);
    }
}
