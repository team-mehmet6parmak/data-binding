package com.mehmet6parmak.training.databinding.observables;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.databinding.ActivityObservablesBinding;

public class ObservablesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityObservablesBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_observables);

        binding.setLifecycleOwner(this);
        binding.setViewModel(new ObservablesViewModel());
    }
}
