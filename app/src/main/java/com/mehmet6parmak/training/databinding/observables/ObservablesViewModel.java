package com.mehmet6parmak.training.databinding.observables;

import androidx.lifecycle.MutableLiveData;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.mehmet6parmak.training.databinding.BR;

public class ObservablesViewModel extends BaseObservable {

    public final ObservableInt someInteger = new ObservableInt(25);
    public final MutableLiveData<Integer> someIntegerLiveData = new MutableLiveData<>();
    public final ObservableField<String> someString = new ObservableField<>("Mehmet");

    public final MutableLiveData<String> anotherString = new MutableLiveData<>();

    private String firstName = "";
    private String lastName = "";

    public ObservablesViewModel() {
        someInteger.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                someIntegerLiveData.postValue(someInteger.get());
            }
        });
    }


    @Bindable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(BR.firstName);

        anotherString.postValue(firstName);
    }

    @Bindable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(BR.lastName);
    }

    @Bindable({"firstName", "lastName"})
    public String getName() {
        return String.format("%s %s", firstName, lastName);
    }

}
