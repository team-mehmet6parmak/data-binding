package com.mehmet6parmak.training.databinding.simple;

import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.mehmet6parmak.training.databinding.R;
import com.mehmet6parmak.training.databinding.databinding.ActivitySimpleBindingBinding;
import com.mehmet6parmak.training.databinding.common.model.Note;
import com.mehmet6parmak.training.databinding.common.model.User;

public class SimpleBindingActivity extends AppCompatActivity {

    private User user = new User("Mehmet", "Altiparmak", 25);
    private Note note = new Note("Layout expressions should be kept small and simple, as they can't be unit tested and have limited IDE support. In order to simplify layout expressions, you can use custom binding adapters.");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //region View Inflation
        ActivitySimpleBindingBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_simple_binding);

        //Another way to inflate
        //ViewDataBinding viewDataBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_simple_binding, null, false);
        //setContentView(viewDataBinding.getRoot());

        //Another way to inflate
        //ActivityUserBinding binding = ActivityUserBinding.inflate(getLayoutInflater());
        //setContentView(binding.getRoot());

        //endregion

        //region Binding data
        binding.setUser(user);
        binding.setSimpleBindingViewModel(new SimpleBindingViewModel(user));
        binding.setNote(note);

        //Dynamically setting the data without knowing its data type.
        //binding.setVariable(BR.user, user);

        //endregion

        //region Typed View references

        CharSequence text = binding.txtFullname.getText();
        Drawable drawable = binding.note.imgStar.getDrawable();

        binding.note.setNote(note); //bind data

        //endregion
    }
}
