package com.mehmet6parmak.training.databinding.simple;

import android.text.TextUtils;
import android.view.View;

import com.mehmet6parmak.training.databinding.common.model.Note;
import com.mehmet6parmak.training.databinding.common.model.User;

public class SimpleBindingViewModel {

    private User user;


    private boolean withName;

    public SimpleBindingViewModel(User user) {
        this.user = user;
        this.username = user.getFirstName();
        this.withName = !TextUtils.isEmpty(user.getFirstName());
    }

    public String username;

    public String getName() {
        return String.format("%s %s", user.getFirstName(), user.getLastName());
    }

    public boolean isWithName() {
        return withName;
    }

    public String getAge() {
        return String.format("Age: %d", user.getAge());
    }

    public View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    public void onNextPageClicked() {
    }

    public void onNextPageClicked(Note note) {
    }

    public void onNextPageClicked(View view) {
    }

    public void onNextPageClicked(View view, Note note) {
    }

    public void onTextChanged(View view, String text) {}
}
