package com.mehmet6parmak.training.databinding.twoway;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.mehmet6parmak.training.databinding.BR;

public class TwoWayBindingViewModel extends BaseObservable {

    public int someIntegerToBeDisplayed = 25;
    private String boundString = "";

    @Bindable
    public String getBoundString() {
        return boundString;
    }

    public void setBoundString(String boundString) {
        this.boundString = boundString;
        notifyPropertyChanged(BR.boundString);
    }

    public void onSubmitClicked() {
        //See bound string is equal to EditText's text.
    }

}
